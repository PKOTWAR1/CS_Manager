const express = require('express');
const cors = require('cors');
const user = require('./src/services/user/user_op');
const pages = require('./src/Config/pages');
const path = require('path');
const cookieParser = require('cookie-parser');
const session = require('express-session');
const menu = require('./src/services/menu/menus');
const uploads = require('./src/services/upload/facility_type_upload');
const colors = require('colors');
const dotenv = require('dotenv').config();

let port = process.env.PORT
let apiroot = '/api';
let app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(express.static('src/public'));
app.use(cors());
app.use(cookieParser());
app.use(session({
    key:'user',
    secret:'TU2UKCVZ4ZYQDO9CV4B',
    resave:false,
    saveUninitialized:false,
    cookie  :{
        expires:3500000
    }
}));


app.use(apiroot, user);
app.use(apiroot, pages);
app.use(apiroot,menu);
app.use(apiroot,uploads);
// app.use(apiroot,express.static(getDir() + '/public'));
// console.log(getDir());
// function getDir() {
//     if (process.pkg) {
//         return path.resolve(process.execPath + "/..");
//     } else {
//         return path.join(require.main ? require.main.path : process.cwd());
//     }
// }

app.listen(port, () => { console.log(`http://localhost:${port}`) });
