const express = require('express');
const router = express.Router();
const connection = require('../Connection/db');
const path = require('path');


let sessionCheck = (req,res,next)=>{
    // console.log(`Sesion Checker : ${JSON.stringify(req.session.user)}`);

    console.log(`Sesion Checker :`);

    if(req.session.user){
        console.log('Found User Session');
        req.session.save();
        next();
    }else{
        console.log('No User Session Found');
        res.redirect('/api/logout');
    }
}

let destroySession = (req,res,next)=>{
        console.log('Destroy Session');
        req.session.destroy();
        next();
}

function time() {
    return ('0' + (new Date().getHours())).slice(-2) + ":"
        + ('0' + (new Date().getMinutes())).slice(-2) + ":"
        + ('0' + (new Date().getSeconds())).slice(-2);
}

function date() {
    return new Date().getFullYear() + '-'
        + ('0' + (new Date().getMonth() + 1)).slice(-2) + '-'
        + ('0' + new Date().getDate()).slice(-2)
        ;
}

function info(...args){
    console.log('Info | '.blue,date(),'-',time(),' | Msg - '.blue,args);
}
function warning(...args){
    console.log('Warning | '.yellow,date(),'-',time(),' | Msg - '.yellow,args);
}
function success(...args){
    console.log('Success | '.green,date(),'-',time(),' | Msg - '.green,args);

}
function errorlog(...args){
    console.log('Error | '.red,date(),'-',time(),' | Msg - '.red,args);

}

module.exports={express,router,connection,path,sessionCheck,destroySession,info,warning,success,errorlog};