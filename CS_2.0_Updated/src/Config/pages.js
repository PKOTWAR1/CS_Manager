const { express, router, path,sessionCheck,destroySession } = require('./app');
const fs = require('fs');


router.get('/dashboard',sessionCheck, function (req, res) {
    // console.log(path.join(__dirname));
    return res.sendFile(`${path.join(__dirname, '../../src/public/dashboard.html')}`);
});
router.get('/dashboard2',sessionCheck, function (req, res) {
    // console.log(path.join(__dirname));
    return res.sendFile(`${path.join(__dirname, '../../src/public/dashboard2.html')}`);
});
router.get('/dashboard3',sessionCheck, function (req, res) {
    // console.log(path.join(__dirname));
    return res.sendFile(`${path.join(__dirname, '../../src/public/columnChart.html')}`);
});
router.get('/add_user',sessionCheck, function (req, res) {
    // console.log(path.join(__dirname));
    return res.sendFile(`${path.join(__dirname, '../../src/public/addUser.html')}`);
});
router.get('/header', function (req, res) {
    // console.log(path.join(__dirname));
    return res.sendFile(`${path.join(__dirname, '../../src/public/header.html')}`);
});
router.get('/loginheader', function (req, res) {
    // console.log(path.join(__dirname));
    return res.sendFile(`${path.join(__dirname, '../../src/public/loginheader.html')}`);
});

router.get('/footer', function (req, res) {
    // console.log(path.join(__dirname));
    return res.sendFile(`${path.join(__dirname, '../../src/public/footer.html')}`);
});

router.get('/add_project',sessionCheck, function (req, res) {
    // console.log(path.join(__dirname));
    return res.sendFile(`${path.join(__dirname, '../../src/public/addProject.html')}`);
});
router.get('/add_site',sessionCheck, function (req, res) {
    // console.log(path.join(__dirname));
    return res.sendFile(`${path.join(__dirname, '../../src/public/addSite.html')}`);
});

router.get('/add_facility',sessionCheck, function (req, res) {
    // console.log(path.join(__dirname));
    return res.sendFile(`${path.join(__dirname, '../../src/public/addFacility.html')}`);
});

router.get('/edit_user',sessionCheck, function (req, res) {
    // console.log(path.join(__dirname));
    return res.sendFile(`${path.join(__dirname, '../../src/public/editUserlist.html')}`);
});

router.get('/editUser',sessionCheck, function (req, res) {
    // console.log(path.join(__dirname));
    // return res.sendFile(`${path.join(__dirname, '../../src/public/editUser1.html')}`);
    let emailId = req.query.data;
    userTemp = fs.readFileSync(`${path.join(__dirname, '../../src/public/editUser.html')}`);
    newTemp = userTemp.toString().replace('emailid',emailId);
    res.send(`${newTemp}`);

});

 router.get('/edit_project',sessionCheck, function (req, res) {
    // console.log(path.join(__dirname));
    return res.sendFile(`${path.join(__dirname, '../../src/public/editProjectlist.html')}`);
});

router.get('/editProject',sessionCheck, function (req, res) {
    // return res.sendFile(`${path.join(__dirname, '../../src/public/editProject.html')}`);
    let projectId = req.query.data;
    projectTemp = fs.readFileSync(`${path.join(__dirname,'../../src/public/editProject.html')}`);
    newTemp = projectTemp.toString().replace('projectid',projectId);
    res.send(`${newTemp}`);
});
router.get('/edit_site',sessionCheck, function (req, res) {
    // console.log(path.join(__dirname));
    return res.sendFile(`${path.join(__dirname, '../../src/public/editSitelist.html')}`);
});

// router.get('/editSite',sessionCheck, function (req, res) {
//     // console.log(path.join(__dirname));
//     let siteId = req.query.data;
//     siteTemp = fs.readFileSync(`${path.join(__dirname, '../../src/public/editSite.html')}`);
//     newTemp = siteTemp.toString().replace('siteid',siteId);
//     res.send(`${newTemp}`);
// });

router.get('/edit_facility',sessionCheck, function (req, res) {
    // console.log(path.join(__dirname));
    return res.sendFile(`${path.join(__dirname, '../../src/public/editFacility.html')}`);
});

router.get('/upload_restaurant_data',sessionCheck, function (req, res) {
    // console.log(path.join(__dirname));
    return res.sendFile(`${path.join(__dirname, '../../src/public/upload_restaurant_data.html')}`);
});
router.get('/upload_fuel_data',sessionCheck, function (req, res) {
    // console.log(path.join(__dirname));
    return res.sendFile(`${path.join(__dirname, '../../src/public/upload_fuel_data.html')}`);
});

router.get('/logout',destroySession,(req,res)=>{
    return res.sendFile(`${path.join(__dirname,'../../src/public/index.html')}`);
})


router.get('/editSiteList',sessionCheck, function (req, res) {
    // return res.sendFile(`${path.join(__dirname, '../../src/public/editProject.html')}`);
    let site_id = req.query.data;
    projectTemp = fs.readFileSync(`${path.join(__dirname,'../../src/public/editSite.html')}`);
    newTemp = projectTemp.toString().replace('siteId',site_id);
    res.send(`${newTemp}`);
});


module.exports = router;