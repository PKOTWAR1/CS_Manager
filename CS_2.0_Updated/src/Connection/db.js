const mysql = require('mysql');
const dotenv = require('dotenv').config();
const connectionSetting ={
    host :process.env.DB_HOST,
    user:process.env.DB_USER,
    password:process.env.DB_PASSWORD,
    database:process.env.DB_DATABASE,
    dateStrings:true
}
const connection = mysql.createPool(connectionSetting);
connection.getConnection((error)=>{
    if(error){
       return console.log('Connection error - ',error);
    }else{
       return console.log('Connection Done');
    }
});

module.exports = connection;