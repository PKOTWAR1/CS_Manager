const {express,router,connection,path, success, warning, error, info} =require('../../Config/app');

router.post('/restaurant_form_list',(req,res)=>{
    info('restaurant_form_list :: GET');
    let {siteid,date} = req.body;
    const sql = "call `CS_2.0`.select_datewise_facility_fr_summary(?,?);"
    try{
        connection.query(sql,[siteid,date],(error,result)=>{
            if(error){
                warning('restaurant_form_list :: error : ' ,error);
                res.status(400).send('restaurant_form_list_error');
            }else{
                success('restaurant_form_list :: result : ' ,result[0]);
                res.status(200).send(result[0]);
            }
        });
    }catch(err){
        errorlog('restaurant_form_list :: error : ' ,error);
        res.status(400).send('restaurant_form_list_error');
    }
});


router.post('/restaurant_form_upload',(req,res)=>{
    info('restaurant_form_upload :: GET');
    let {date,facilityid,mrp_amt,non_mrp_amt} = req.body;
    const sql = "call `CS_2.0`.insert_data_to_fr_summary(?,?,?,?);"
    try{
        connection.query(sql,[date,facilityid,mrp_amt,non_mrp_amt],(error,result)=>{
            if(error){
                warning('restaurant_form_upload :: error : ' ,error);
                res.status(400).send('restaurant_form_upload_error');
            }else{
                success('restaurant_form_upload :: result : ' ,result);
                res.status(200).send(result);
            }
        });
    }catch(err){
        errorlog('restaurant_form_upload :: error : ' ,error);
        res.status(400).send('restaurant_form_upload_error');
    }
});


router.post('/fuel_form_list',(req,res)=>{
    info('fuel_form_list :: GET');
     
    let {siteid,date} = req.body;
    const sql = "call `CS_2.0`.select_datewise_Fuel_Summary_form(?,?) "
    try{
        connection.query(sql,[siteid,date],(error,result)=>{
            if(error){
                warning('fuel_form_list :: error : ' ,error);
                res.status(400).send('restaurant_form_list_error');
            }else{
                success('fuel_form_list :: result : ' ,result[0]);
                res.status(200).send(result[0]);
            }
        });
    }catch(err){
        errorlog('fuel_form_list :: error : ' ,error);
        res.status(400).send('fuel_form_list_error');
    }
});


router.post('/fuel_form_upload',(req,res)=>{
    info('restaurant_form_upload :: GET');

    let { site_id,date,Fuel_Type_Id,in_qty,in_rate,in_amt,in_margin} = req.body;
    
    const sql = "call `CS_2.0`.insert_into_Fuel_Summary(?,?,?,?,?,?,?);"
    try{
        connection.query(sql,[site_id,date,Fuel_Type_Id,in_qty,in_rate,in_amt,in_margin],(error,result)=>{
            if(error){
                warning('restaurant_form_upload :: error : ' ,error);
                res.status(400).send('restaurant_form_upload_error');
            }else{
                success('restaurant_form_upload :: result : ' ,result);
                res.status(200).send(result);
            }
        });
    }catch(err){
        errorlog('restaurant_form_upload :: error : ' ,error);
        res.status(400).send('restaurant_form_upload_error');
    }
});


module.exports=router;