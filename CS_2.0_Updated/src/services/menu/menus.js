const {express,router,connection,path, warning, success, errorlog, info} =require('../../Config/app');

router.get('/getmenu',(req,res)=>{
    let menu_privileges = req.session.user[0].menu_privileges;
    info('getmenu :: GET',menu_privileges.trim());
    // console.log('session  :- ',req.session.user[0].first_name);
    let sql = "call `CS_2.0`.select_menus(?);";
    try{
        connection.query(sql,[menu_privileges.trim()],(error,result)=>{
            if(error){
                warning('Connection - ERROR : '.red,error);
                res.status(400).send('MYSQL-CONNECTION-REFUSED');
            }else{
                success('get-menus :: result',result[0]);
                res.status(200).send(result);
            }
        });
    }catch(er){
        errorlog('get-menu : Catch'.red);
        res.status(400).send('getmenu-Error');
    }


});

router.get('/getmenu_list',(req,res)=>{
    info('getmenu_list :: GET');
    let sql = "call `CS_2.0`.get_all_menuList();";
    try{
        connection.query(sql,(error,result)=>{
            if(error){
                warning('getmenu_list :: error : ' ,error);
                res.status(400).send('getmenu_list_error');
            }else{
                success('getmenu_list :: result - ',result);
                res.status(200).send(result[0]);
            }
        });
    }catch(err){
        errorlog('getmenu_list :: catch : ' ,err);
        res.status(400).send('getmenu_list_catch');
    }
});

router.get('/getsite_list',(req,res)=>{
    info('getsite_list :: GET');
    let sql = "call `CS_2.0`.get_site_list();";
    try{
        connection.query(sql,(error,result)=>{
            if(error){
                warning('getsite_list :: error : ' ,error);
                res.status(400).send('getsite_list_error');
            }else{
                success('getsite_list :: result - ',result);
                res.status(200).send(result[0]);
            }
        });
    }catch(err){
        errorlog('getsite_list :: catch : ' ,err);
        res.status(400).send('getsite_list_catch');
    }
});

router.get('/get_userwise_site_list',(req,res)=>{
    info('get_userwise_site_list :: table');
    let user_site_list = req.session.user[0].site_privileges;
    const sql="call `CS_2.0`.get_userwise_site_list(?);";
    try{
        connection.query(sql,[user_site_list],(error,result)=>{
            if(error){
                warning('get_userwise_site_list_error',error);
                res.status(400).send('get_userwise_site_list_error ');
            }else{
                success('get_userwise_site_list_RESULT : ',result[0]);
                res.status(200).send(result[0]);
            }
        });
    }catch(err){
        errorlog('get_userwise_site_list_Catch',error);
        res.status(400).send('get_userwise_site_list_ERROR ');
    }
});

router.get('/getProject_list',(req,res)=>{
    info('getProject_list :: GET');
    let sql = "call `CS_2.0`.get_project_list();";
    try{
        connection.query(sql,(error,result)=>{
            if(error){
                warning('getProject_list :: error : ' ,error);
                res.status(400).send('getProject_list_error');
            }else{
                success('getProject_list :: result - ',result);
                res.status(200).send(result[0]);
            }
        });
    }catch(err){
        errorlog('getProject_list :: catch : ' ,err);
        res.status(400).send('getProject_list_catch');
    }
});

router.get('/getfacility_list',(req,res)=>{
    info('getfacility_list :: GET');
    let sql = "call `CS_2.0`.get_facility_list();";
    try{
        connection.query(sql,(error,result)=>{
            if(error){
                    warning('getfacility_list :: error : ' ,error);
                    res.status(400).send('getfacility_list_error');
            }else{
                success('getfacility_list :: result - ',result);
                res.status(200).send(result[0]);
            }
        });
    }catch(err){
        errorlog('getfacility_list :: catch : ' ,err);
        res.status(400).send('getfacility_list_catch');
    }
});


router.get('/getfacility_status',(req,res)=>{
    info('getfacility_status :: GET');
    let sql = "call `CS_2.0`.get_facility_status();";
    try{
        connection.query(sql,(error,result)=>{
            if(error){
                    warning('getfacility_status :: error : ' ,error);
                    res.status(400).send('getfacility_status_error');
            }else{
                success('getfacility_status :: result - ',result);
                res.status(200).send(result[0]);
            }
        });
    }catch(err){
        errorlog('getfacility_status :: catch : ' ,err);
        res.status(400).send('getfacility_status_catch');
    }
});


module.exports = router;