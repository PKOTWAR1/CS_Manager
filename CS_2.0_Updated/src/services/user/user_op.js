const session = require('express-session');
const {router,connection,info,warning,success,errorlog} = require('../../Config/app');
router.post('/login',(req,res)=>{
    // console.log('login :: get');
    info('login :: get');
    let {userId,pass} = req.body;

        const sql = 'call `CS_2.0`.login_check(?,?);'
    try{
        connection.query(sql,[userId,pass],(error,result)=>{
            if(error){
                // console.log('Login error'.red,error);
                warning('Login error',error);
                res.status(400).send(error);
            }else{
                success('Login - `',result[0]);
                // console.log(result[0]);
              req.session.user = result[0];
            //   console.log('req.session : '.green,req.session.user);
            //req.session.save();
                res.status(200).send(result[0]);
            }
        })
    }catch(e){
        // console.log('catch');
        errorlog('Login : catch');
        res.status(400).send('Error Occured');
    }

});


router.get('/loggedin_user',(req,res)=>{
    // console.log('loggedin User :: get');
    info('loggedin User :: get');

    try{
        // console.log('session  :- ',req.session.user[0].first_name);
        info('session  :- ',req.session.user[0].first_name);
        let user = req.session.user[0].first_name;
        res.status(200).send(user);

    }catch(e){
        // console.log('catch');
        errorlog('loggedin_user : catch');

        res.status(400).send('null');
    }

});









router.post('/add_newuser',(req,res)=>{
    // console.log('add_newuser :: POST');
    info('add_newuser :: POST');
    let {fname,lname,email,pass,mobile,location,city,state,status,site_p,menu_p} = req.body;
   
    // console.log(fname,lname,email,pass,mobile,location,city,state,status,site_p,menu_p);
    info(fname,lname,email,pass,mobile,location,city,state,status,site_p,menu_p);

    let sql = "call `CS_2.0`.insert_into_user_master(?,?,?,?,?,?,?,?,?,?,?);"
    try{
        connection.query(sql,
        [fname,lname,email,pass,mobile,location,state,city,status,site_p,menu_p],
        (error,result)=>{
            if(error){
                // console.log('add_newuser :: error ',error);
                warning('add_newuser :: error ',error);
                res.status(400).send('add_newuser_error');
            }else{
                // console.log('add_newuser  :: result ',result.affectedRows);
                success('add_newuser  :: result ',result.affectedRows);
                res.status(200).send(result);
            }
            });
    }catch(err){
        // console.log('add_neruser :: catch',err);
        errorlog('add_neruser :: catch',err);
        res.status(400).send('add_newuser_catch');

    }
});





router.post('/add_newproject',(req,res)=>{
    // console.log('add_newproject :: post');
    info('add_newproject :: post');
    let {pname,ptype,pnh,pstate,pdistrict,plength,sdate,edate,psch,pech,psglt,psglg,peglt,peglg} = req.body;
    info(pname,ptype,pnh,pstate,pdistrict,plength,sdate,edate,psch,pech,psglt,psglg,peglt,peglg);
    // console.log(pname,ptype,pnh,pstate,pdistrict,plength,sdate,edate,psch,pech,psglt,psglg,peglt,peglg);
    let sql ="call `CS_2.0`.insert_into_project_master(?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
    try{
        connection.query(sql,[pname,ptype,pnh,pstate,pdistrict,plength,sdate,edate,psch,pech,psglt,psglg,peglt,peglg],(error,result)=>{
            if(error){
                warning('add_newproject_error',error);
                // console.log('add_newproject_error',error);
                res.status(400).send('add_newproject_error');
            }else{
                success('add_newproject_result',result);
                // console.log('add_newproject_result',result);
                res.status(200).send(result);
            }
        });
    }catch(err){
        errorlog('add_newproject_error : catch',err);
        // console.log('add_newproject_error : catch',err);
        res.status(400).send(err); 
    }
});


router.post('/add_site',(req,res)=>{
    // console.log('add_site :: post');
    info('add_site :: post');
    let {project_name,site_name,ERP_site,Chainage,Geo_Latitude,Geo_Longitude,Site_Manager_Name,Site_Manager_Mobile,IT_Incharge_Name,IT_Incharge_Mobile} = req.body;
    // console.log("project_name ::" + project_name + " / site_id ::" + site_id + " / site_name ::" + site_name + " / ERP_name ::" + ERP_site + " /Chainage ::" + Chainage +
    // " /Geo_Latitude ::" + Geo_Latitude + " /Geo_Longitude ::" + Geo_Longitude + " / Site_Manager_Name ::" + Site_Manager_Name + " /Site_Manager_Mobile ::" + Site_Manager_Mobile +
    // " /IT_Incharge_Mobile ::" + IT_Incharge_Name + "/IT_Incharge_Mobile ::" + IT_Incharge_Mobile);
    
    let data=[project_name,site_name,ERP_site,Chainage,Geo_Latitude,Geo_Longitude,Site_Manager_Name,Site_Manager_Mobile,IT_Incharge_Name,IT_Incharge_Mobile]
   
   info("data values",data);
//    console.log("data values",data);
   let sql ="call `CS_2.0`.insert_into_site_master(?,?,?,?,?,?,?,?,?,?);";
    try{
        connection.query(sql,data,(error,result)=>{
            if(error){
                warning('add_site_error',error);
                // console.log('add_site_error',error);
                res.status(400).send('add_site_error');
            }else{
                success('add_site_result - ',result);
                res.status(200).send(result);
            }
        });
    }catch(err){
        errorlog("add_site - catch error");
        res.status(400).send('catch err');

    }
});

router.post('/add_facility',(req,res)=>{
    info('add_facility :: POST');    
     let {site,ftype,opname,outlatename,mg,nonmrp_rev_share,mrp_rev_share,nhai_share,sdate,end_date,status}=req.body;
     let data=[site,ftype,opname,outlatename,mg,nonmrp_rev_share,mrp_rev_share,nhai_share,sdate,end_date,status];
    const sql = "call `CS_2.0`.insert_into_facility_master(?,?,?,?,?,?,?,?,?,?,?);";
    try{
        connection.query(sql,data,(error,result)=>{
            if(error){
                warning('add_facility_error',error);
                res.status(400).send('add_facility_error');
            }else{
                success('add_facility_RESULT : ',result);
                res.status(200).send(result);
            }
        });
    }catch(err){
        errorlog('add_facility :: catch ',err);
        res.status(400).send('add_facility_ERROR');
    }
});



router.get('/get_facility/:siteId',(req,res)=>{
    let siteid  =req.params.siteId;
    info('get_facility :: GET ',siteid);    
    const sql = "call `CS_2.0`.get_facilityData_BySiteId(?);";
    try{
        connection.query(sql,[siteid],(error,result)=>{
            if(error){
                warning('get_facility_error',error);
                res.status(400).send('get_facility_error');
            }else{
                result('get_facility_RESULT : ',result[0]);
                res.status(200).send(result[0]);
            }
        });
    }catch(err){
        errorlog('get_facility :: catch ',err);
        res.status(400).send('get_facility_ERROR');
    }
});

router.post('/update_facility',(req,res)=>{
    info('update_facility :: POST');    
    let {site,ftype,opname,outlatename,mg,nonmrp_rev_share,mrp_rev_share,nhai_share,sdate,end_date,fid,status}=req.body;
    let data=[site,ftype,opname,outlatename,mg,nonmrp_rev_share,mrp_rev_share,nhai_share,sdate,end_date,fid,status];
    info('update_facility - ',data);
    const sql = "call `CS_2.0`.update_facility_data(?,?,?,?,?,?,?,?,?,?,?,?);";
    try{
        connection.query(sql,data,(error,result)=>{
            if(error){
                warning('update_facility_error',error);
                res.status(400).send('update_facility_error');
            }else{
                success('update_facility_RESULT : ',result);
                res.status(200).send(result);
            }
        });
    }catch(err){
        errorlog('update_facility :: catch ',err);
        res.status(400).send('update_facility_ERROR');
    }
});

router.get('/get_facility_byFid/:facilityId',(req,res)=>{
    let facilityid  =req.params.facilityId;
    info('get_facility-2 :: GET ',facilityid);    
    const sql = "call `CS_2.0`.get_facilitydata_ByFId(?);";
    try{
        connection.query(sql,[facilityid],(error,result)=>{
            if(error){
                warning('get_facility_error',error);
                res.status(400).send('get_facility_error');
            }else{
                success('get_facility_RESULT : ',result[0]);
                res.status(200).send(result[0]);
            }
        });
    }catch(err){
        errorlog('get_facility :: catch ',err);
        res.status(400).send('get_facility_ERROR');
    }
});


router.get('/get_userlist',(req,res)=>{
    info('get_userlist :: EDIT');
    const sql="call `CS_2.0`.get_user_data();";
    try{
        connection.query(sql,(error,result)=>{
            if(error){
                warning('get_userlist_error',error);
                res.status(400).send('get_userlist_error ');
            }else{
                success('get_userlist_RESULT : ',result[0]);
                res.status(200).send(result[0]);
            }
        });
    }catch(err){
        errorlog('get_userlist_Catch',error);
        res.status(400).send('get_userlist_ERROR ');
    }
});

router.get('/getUserData/:mailid',(req,res)=>{
    let email_Id = req.params.mailid;
    info('getUserData :: GET');
    const sql ="call `CS_2.0`.get_userdata_ById(?);";
    try{
        connection.query(sql,[email_Id],(error,result)=>{
            if(error){
                warning('getUserData :: error',error);
                res.status(400).send('getUserData-error');
            }else{
                success('getUserData :: RESULT',result[0]);
                res.status(200).send(result[0]);
            }
        })
    }catch(err){
        errorlog('getUserData :: CATCH',err);
        res.status(400).send('getUserData-ERROR');
    }
});


router.get('/get_projectlist',(req,res)=>{
    info('get_projectlist :: EDIT');
    const sql="call `CS_2.0`.get_project_data();";
    try{
        connection.query(sql,(error,result)=>{
            if(error){
                warning('get_projectlist_error',error);
                res.status(400).send('get_projectlist_error ');
            }else{
                success('get_projectlist_RESULT : ',result[0]);
                res.status(200).send(result[0]);
            }
        });
    }catch(err){
        errorlog('get_projectlist_Catch',error);
        res.status(400).send('get_projectlist_ERROR ');
    }
});

router.get('/getProjectData/:project_name',(req,res)=>{
    let projectName = req.params.project_name;
    info('getProjectData :: GET');
    const sql ="call `CS_2.0`.get_projectdata_ByName(?);";
    try{
        connection.query(sql,[projectName],(error,result)=>{
            if(error){
                warning('getProjectData :: error',error);
                res.status(400).send('getProjectData-error');
            }else{
                success('getProjectData :: RESULT',result[0]);
                res.status(200).send(result[0]);
            }
        })
    }catch(err){
        errorlog('getProjectData :: CATCH',err);
        res.status(400).send('getProjectData-ERROR');
    }
});


router.get('/get_site_list',(req,res)=>{
    info('get_site_list :: table');
    const sql="call `CS_2.0`.get_site_data();";
    try{
        connection.query(sql,(error,result)=>{
            if(error){
                warning('get_site_ist_error',error);
                res.status(400).send('get_site_list_rror ');
            }else{
                success('get_site_RESULT : ',result[0]);
                res.status(200).send(result[0]);
            }
        });
    }catch(err){
        errorlog('get_site_list_Catch',error);
        res.status(400).send('get_site_list_ERROR ');
    }
});


router.get('/getSitetData/:site1',(req,res)=>{
    let siteid1 = req.params.site1;
    info('getSiteData :: GET'+siteid1);
    const sql ="call `CS_2.0`.select_siteData_byId(?)";
    try{
        connection.query(sql,[siteid1],(error,result)=>{
            if(error){
                warning('getSiteData :: error',error);
                res.status(400).send('getSiteData-error');
            }else{
                success('getSiteData :: RESULT',result[0]);
                res.status(200).send(result[0]);
            }
        });
    }catch(err){
        errorlog('getSiteData :: CATCH',err);
        res.status(400).send('getSiteData-ERROR');
    }
});

router.post('/updateUser',(req,res)=>{
    info('updateuser :: POST');
    let {fname,lname,userid,email,pass,mobile,location,city,state,status,site_p,menu_p} = req.body;
   
    info(fname,lname,userid,email,pass,mobile,location,city,state,status,site_p,menu_p);
    let sql = "call `CS_2.0`.update_user_data(?,?,?,?,?,?,?,?,?,?,?,?)"
    try{
        connection.query(sql,
        [fname,lname,userid,email,pass,mobile,location,state,city,status,site_p,menu_p],
        (error,result)=>{
            if(error){
                warning('updateuser :: error ',error);
                res.status(400).send('updateuser_error');
            }else{
                success('updateuser  :: result ',result.affectedRows);
                res.status(200).send(result);
            }
            });
    }catch(err){
        errorlog('updateuser :: catch',err);
        res.status(400).send('updateuser_catch');

    }
});


router.post('/updateProject',(req,res)=>{
    info('updateProject:: post');
    let {pid,pname,ptype,pnh,pstate,pdistrict,plength,sdate,edate,psch,pech,psglt,psglg,peglt,peglg} = req.body;
    info(pname,ptype,pnh,pstate,pdistrict,plength,sdate,edate,psch,pech,psglt,psglg,peglt,peglg);
    let sql ="call `CS_2.0`.update_project_data(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
    try{
        connection.query(sql,[pid,pname,ptype,pnh,pstate,pdistrict,plength,sdate,edate,psch,pech,psglt,psglg,peglt,peglg],(error,result)=>{
            if(error){
                warning('updateProject_error',error);
                res.status(400).send('updateProject_error');
            }else{
                success('updateProject_result',result);
                res.status(200).send(result);
            }
        });
    }catch(err){
        errorlog('updateProject_error : catch',err);
        res.status(400).send(err); 
    }
});


router.post('/updateSite',(req,res)=>{
    info('updateSite:: post');
    let {project_name,site_id,site_name,ERP_site,Chainage,Geo_Latitude,Geo_Longitude,Site_Manager_Name,Site_Manager_Mobile,IT_Incharge_Name,IT_Incharge_Mobile} = req.body;
    // console.log("project_name ::" + project_name + " / site_id ::" + site_id + " / site_name ::" + site_name + " / ERP_name ::" + ERP_site + " /Chainage ::" + Chainage +
    // " /Geo_Latitude ::" + Geo_Latitude + " /Geo_Longitude ::" + Geo_Longitude + " / Site_Manager_Name ::" + Site_Manager_Name + " /Site_Manager_Mobile ::" + Site_Manager_Mobile +
    // " /IT_Incharge_Mobile ::" + IT_Incharge_Name + "/IT_Incharge_Mobile ::" + IT_Incharge_Mobile);
    
    let data=[project_name,site_id,site_name,ERP_site,Chainage,Geo_Latitude,Geo_Longitude,Site_Manager_Name,Site_Manager_Mobile,IT_Incharge_Name,IT_Incharge_Mobile]
    info("data  updateSite",data);
   let sql ="call `CS_2.0`.update_site_data(?,?,?,?,?,?,?,?,?,?,?)";
    try{
        connection.query(sql,data,(error,result)=>{
            if(error){
                warning('updateSiteerror',error);
                res.status(400).send('updateSite_error');
            }else{
                success(result);
                res.status(200).send(result);
            }
        });
    }catch(err){
        errorlog("catch error");
        res.status(400).send('catch err');

    }
});

module.exports=router;